<%@ page import="com.luxoft.dnepr.courses.unit17.listeners.Constants" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page session="false" %>


<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>User Login</title>
</head>
<body >
<form method="POST" action="/login">
    <TABLE>
        <TR>
            <TD>UserName:</TD>
            <TD><input type="text" name= <%= Constants.LOGIN  %>  ></TD>
            <TD> <div style="color:red">
                <% if (request.getParameter("error")!=null){
                out.print("Wrong login or password");
                 } %>
            </div></TD>
        </TR>
        <TR>
            <TD>Password:</TD> <TD ><input type="Password" name= <%= Constants.PASSWORD %> ></TD>
        </TR>
        <TR>
            <TD><input type="submit" value="Submit" name="submit"></TD>
        </TR>
    </TABLE>
</form>
</body>
</html>
