package com.luxoft.dnepr.courses.unit17.servlets;

import com.luxoft.dnepr.courses.unit17.listeners.Constants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class LogoutServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getSession(false) != null){
            request.getSession(false).invalidate();
        }
        response.sendRedirect(Constants.LOGIN_PAGE);
    }
}
