package com.luxoft.dnepr.courses.unit17.servlets;

import com.luxoft.dnepr.courses.unit17.listeners.Constants;
import com.luxoft.dnepr.courses.unit17.model.SessionData;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;

public class SessionDataServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        SessionData data = new SessionData();

        data.setActiveSessions(((AtomicLong) getServletContext()
                .getAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE)).get());
        data.setActiveSessionsAdmins(((AtomicLong) getServletContext()
                .getAttribute(Constants.ACTIVE_SESSION_ADMIN_ATTRIBUTE)).get());
        data.setActiveSessionsUsers(((AtomicLong) getServletContext()
                .getAttribute(Constants.ACTIVE_SESSION_USER_ATTRIBUTE)).get());

        data.setTotalHttpRequests(((AtomicLong) getServletContext()
                .getAttribute(Constants.HTTP_REQUESTS)).get());
        data.setTotalGetHttpRequests(((AtomicLong) getServletContext()
                .getAttribute(Constants.HTTP_GET_REQUESTS)).get());
        data.setTotalPostHttpRequests(((AtomicLong) getServletContext()
                .getAttribute(Constants.HTTP_POST_REQUESTS)).get());
        data.setTotalOtherHttpRequests(((AtomicLong) getServletContext()
                .getAttribute(Constants.HTTP_OTHER_REQUESTS)).get());

        request.setAttribute(Constants.SESSION_DATA, data);
        RequestDispatcher view = request.getRequestDispatcher("WEB-INF/jsp/admin/sessionData.jsp");
        view.forward(request, response);
    }
}
