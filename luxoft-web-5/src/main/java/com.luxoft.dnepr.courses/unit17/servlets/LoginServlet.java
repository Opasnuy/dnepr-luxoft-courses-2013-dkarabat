package com.luxoft.dnepr.courses.unit17.servlets;


import com.luxoft.dnepr.courses.unit17.listeners.Constants;
import com.luxoft.dnepr.courses.unit17.model.User;
import com.luxoft.dnepr.courses.unit17.utils.UserUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


public class LoginServlet extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String login = request.getParameter(Constants.LOGIN);
        String password = request.getParameter(Constants.PASSWORD);
        UserUtil.getUsersFromJSON(getServletContext());
        User user = new User(login, password);
        if (UserUtil.isRegisteredUser(user)) {
            HttpSession session = request.getSession(true);
            session.setAttribute(Constants.LOGIN, request.getParameter(Constants.LOGIN));
            session.setAttribute(Constants.ROLE, UserUtil.getUsersRole(request.getParameter(Constants.LOGIN)));
            response.sendRedirect(getServletContext().getContextPath()+Constants.USER_PAGE);
        } else {
            response.sendRedirect(getServletContext().getContextPath()+Constants.LOGIN_ERROR_PAGE);
        }
    }


}