package com.luxoft.dnepr.courses.unit17.listeners;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.luxoft.dnepr.courses.unit17.model.User;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

public class ApplicationContextListener implements ServletContextListener {



    @Override
    public void contextInitialized(ServletContextEvent sce) {

        sce.getServletContext().setAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE, new AtomicLong(0));
        sce.getServletContext().setAttribute(Constants.ACTIVE_SESSION_ADMIN_ATTRIBUTE, new AtomicLong(0));
        sce.getServletContext().setAttribute(Constants.ACTIVE_SESSION_USER_ATTRIBUTE, new AtomicLong(0));

        sce.getServletContext().setAttribute(Constants.HTTP_REQUESTS, new AtomicLong(0));
        sce.getServletContext().setAttribute(Constants.HTTP_GET_REQUESTS, new AtomicLong(0));
        sce.getServletContext().setAttribute(Constants.HTTP_POST_REQUESTS, new AtomicLong(0));
        sce.getServletContext().setAttribute(Constants.HTTP_OTHER_REQUESTS, new AtomicLong(0));

        String json = sce.getServletContext().getInitParameter(Constants.USERS_MAP);
        Gson gson = new Gson();
        Set<User>  users = gson.fromJson(json, new TypeToken<Set<User>>() {
        }.getType());
        sce.getServletContext().setAttribute(Constants.USERS_MAP,users);



    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        sce.getServletContext().removeAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE);
        sce.getServletContext().removeAttribute(Constants.ACTIVE_SESSION_ADMIN_ATTRIBUTE);
        sce.getServletContext().removeAttribute(Constants.ACTIVE_SESSION_USER_ATTRIBUTE);

        sce.getServletContext().removeAttribute(Constants.HTTP_REQUESTS);
        sce.getServletContext().removeAttribute(Constants.HTTP_GET_REQUESTS);
        sce.getServletContext().removeAttribute(Constants.HTTP_POST_REQUESTS);
        sce.getServletContext().removeAttribute(Constants.HTTP_OTHER_REQUESTS);
    }
}
