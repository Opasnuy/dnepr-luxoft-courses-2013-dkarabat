package com.luxoft.dnepr.courses.unit17.listeners;

public interface Constants {
    public String ACTIVE_SESSION_ATTRIBUTE = "ACTIVE_SESSION";
    public String ACTIVE_SESSION_ADMIN_ATTRIBUTE = "ACTIVE_ADMIN_SESSION";
    public String ACTIVE_SESSION_USER_ATTRIBUTE = "ACTIVE_USER_SESSION";

    public String HTTP_REQUESTS = "TOTAL_HTTP_REQUESTS";
    public String HTTP_POST_REQUESTS = "TOTAL_HTTP_POST_REQUESTS";
    public String HTTP_GET_REQUESTS = "TOTAL_HTTP_GET_REQUESTS";
    public String HTTP_OTHER_REQUESTS = "TOTAL_HTTP_OTHER_REQUESTS";

    public String USERS_MAP = "users";
    public String SESSION_DATA = "data";

    public String LOGIN_ERROR_PAGE = "/index.html?error=1";
    public String LOGIN_PAGE = "/index.html";
    public String USER_PAGE = "/user";
    public String ADMIN_PAGE = "/admin/sessionData";

    public String LOGIN = "login";
    public String PASSWORD = "password";
    public String ROLE = "role";
    public String ADMIN_ROLE = "admin";
    public String USER_ROLE = "user";
}
