package com.luxoft.dnepr.courses.unit17.listeners;

import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import java.util.concurrent.atomic.AtomicLong;

public class AttributeSessionCountListener implements HttpSessionAttributeListener {
    @Override
    public void attributeAdded(HttpSessionBindingEvent sbe) {
        String role = (String)sbe.getSession().getAttribute(Constants.ROLE);
        if(role.equals(Constants.ADMIN_ROLE))
            getActiveAdminSessions(sbe).getAndIncrement();
        else if(role.equals(Constants.USER_ROLE))
            getActiveUserSessions(sbe).getAndIncrement();
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent sbe) {
    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent sbe) {

    }
    private AtomicLong getActiveAdminSessions(HttpSessionBindingEvent sbe) {
        return (AtomicLong) sbe.getSession().getServletContext().getAttribute(Constants.ACTIVE_SESSION_ADMIN_ATTRIBUTE);
    }
    private AtomicLong getActiveUserSessions(HttpSessionBindingEvent sbe) {
        return (AtomicLong) sbe.getSession().getServletContext().getAttribute(Constants.ACTIVE_SESSION_USER_ATTRIBUTE);
    }
}
