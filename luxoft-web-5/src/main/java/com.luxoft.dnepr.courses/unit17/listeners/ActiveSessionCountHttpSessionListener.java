package com.luxoft.dnepr.courses.unit17.listeners;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.concurrent.atomic.AtomicLong;

public class ActiveSessionCountHttpSessionListener implements HttpSessionListener {

    @Override
    public void sessionCreated(HttpSessionEvent hse) {
        getActiveSessions(hse).getAndIncrement();
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent hse) {
        getActiveSessions(hse).getAndDecrement();

        String role = (String) hse.getSession().getAttribute(Constants.ROLE);
        if(role.equals(Constants.ADMIN_ROLE))
            getActiveAdminSessions(hse).getAndDecrement();
        else if(role.equals(Constants.USER_ROLE))
            getActiveUserSessions(hse).getAndDecrement();
    }

    private AtomicLong getActiveSessions(HttpSessionEvent hse) {
        return (AtomicLong) hse.getSession().getServletContext().getAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE);
    }
    private AtomicLong getActiveAdminSessions(HttpSessionEvent hse) {
        return (AtomicLong) hse.getSession().getServletContext().getAttribute(Constants.ACTIVE_SESSION_ADMIN_ATTRIBUTE);
    }
    private AtomicLong getActiveUserSessions(HttpSessionEvent hse) {
        return (AtomicLong) hse.getSession().getServletContext().getAttribute(Constants.ACTIVE_SESSION_USER_ATTRIBUTE);
    }
}
