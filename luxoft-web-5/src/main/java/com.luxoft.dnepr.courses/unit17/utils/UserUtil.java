package com.luxoft.dnepr.courses.unit17.utils;


import com.luxoft.dnepr.courses.unit17.listeners.Constants;
import com.luxoft.dnepr.courses.unit17.model.User;

import javax.servlet.ServletContext;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class UserUtil {


    private static Set<User> users = new HashSet<>();

    public static Set<User> getUsers() {
        return users;
    }

    public static void getUsersFromJSON(ServletContext context) {
        users.addAll((Collection<? extends User>) context.getAttribute(Constants.USERS_MAP));
    }


    public static boolean isRegisteredUser(User user) {
        boolean registered = false;
        if (users.contains(user)) {
            registered = true;
        }
        return registered;
    }

    public static String getUsersRole(String login) {
        for (User user : users) {
            if (user.getLogin().equals(login))
                return user.getRole();
        }
        return "";
    }

}