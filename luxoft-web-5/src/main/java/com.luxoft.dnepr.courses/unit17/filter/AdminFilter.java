package com.luxoft.dnepr.courses.unit17.filter;


import com.luxoft.dnepr.courses.unit17.listeners.Constants;
import com.luxoft.dnepr.courses.unit17.model.User;
import com.luxoft.dnepr.courses.unit17.utils.UserUtil;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Set;

public class AdminFilter implements Filter {
    private Set<User> users;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        UserUtil.getUsersFromJSON(filterConfig.getServletContext());
    }

    @Override
    public void doFilter(ServletRequest servletRequest
            , ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(false);

        if (!isAdmin((String) session.getAttribute(Constants.LOGIN))) {
            response.sendRedirect(Constants.USER_PAGE);
            return;
        }
        filterChain.doFilter(request, response);
    }

    private boolean isAdmin(String login) {
        for (User user : UserUtil.getUsers()) {
            if (user.getLogin().equals(login) && user.getRole().equals(Constants.ADMIN_ROLE)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void destroy() {
    }

}