package com.luxoft.dnepr.courses.unit17.model;

public class SessionData {
   private long ActiveSessions;
   private long ActiveSessionsUsers;
   private long ActiveSessionsAdmins;
   private long TotalHttpRequests;
   private long TotalPostHttpRequests;
   private long TotalGetHttpRequests;
   private long TotalOtherHttpRequests;

    public SessionData() {
    }

    public long getActiveSessions() {
        return ActiveSessions;
    }

    public void setActiveSessions(long activeSessions) {
        ActiveSessions = activeSessions;
    }

    public long getActiveSessionsUsers() {
        return ActiveSessionsUsers;
    }

    public void setActiveSessionsUsers(long activeSessionsUsers) {
        ActiveSessionsUsers = activeSessionsUsers;
    }

    public long getActiveSessionsAdmins() {
        return ActiveSessionsAdmins;
    }

    public void setActiveSessionsAdmins(long activeSessionsAdmins) {
        ActiveSessionsAdmins = activeSessionsAdmins;
    }

    public long getTotalHttpRequests() {
        return TotalHttpRequests;
    }

    public void setTotalHttpRequests(long totalHttpRequests) {
        TotalHttpRequests = totalHttpRequests;
    }

    public long getTotalPostHttpRequests() {
        return TotalPostHttpRequests;
    }

    public void setTotalPostHttpRequests(long totalPostHttpRequests) {
        TotalPostHttpRequests = totalPostHttpRequests;
    }

    public long getTotalGetHttpRequests() {
        return TotalGetHttpRequests;
    }

    public void setTotalGetHttpRequests(long totalGetHttpRequests) {
        TotalGetHttpRequests = totalGetHttpRequests;
    }

    public long getTotalOtherHttpRequests() {
        return TotalOtherHttpRequests;
    }

    public void setTotalOtherHttpRequests(long totalOtherHttpRequests) {
        TotalOtherHttpRequests = totalOtherHttpRequests;
    }
}