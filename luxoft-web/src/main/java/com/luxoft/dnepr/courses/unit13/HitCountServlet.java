package com.luxoft.dnepr.courses.unit13;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.atomic.AtomicInteger;

public class HitCountServlet extends HttpServlet {
    private static AtomicInteger count;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        count.incrementAndGet();
        response.setStatus(HttpServletResponse.SC_OK);
        response.setCharacterEncoding("utf8");
        response.setContentType("application/json");
        StringBuffer emps = new StringBuffer();
        PrintWriter writer = response.getWriter();
        emps.append("{\"hitCount\":");
        emps.append(count);
        emps.append("}");
        response.setContentLength(emps.length());
        writer.println(emps);
    }
}
