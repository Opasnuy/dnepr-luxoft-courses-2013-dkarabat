package com.luxoft.dnepr.courses.unit14;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.luxoft.dnepr.courses.unit14.util.PageBuilder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class LoginServlet extends HttpServlet {

    private Map<String, String> users = new ConcurrentHashMap<>();

    private final boolean REGISTERED = true;
    private final boolean NOT_REGISTERED = false;

    @Override
    public void init() throws ServletException {
        String json = getServletContext().getInitParameter("users");
        Gson gson = new Gson();
        users = gson.fromJson(json, new TypeToken<Map<String, String>>() {
        }.getType());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        StringBuilder content = new StringBuilder("");
        content.append(PageBuilder.getIndexPage(REGISTERED));
        PrintWriter writer = response.getWriter();
        writer.println(content);
    }


    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        StringBuilder content = new StringBuilder("");
        String login = request.getParameter("log");
        String password = request.getParameter("pwd");

        if (isRegisteredUser(login, password)) {
            HttpSession session = request.getSession(true);
            session.setAttribute("login", request.getParameter("log"));
            response.sendRedirect("/users");
        } else {
            content.append(PageBuilder.getIndexPage(NOT_REGISTERED));
            PrintWriter writer = response.getWriter();
            writer.println(content);
        }
    }

    private boolean isRegisteredUser(String login, String password) {
        boolean registered = false;
        if (users.containsKey(login) && users.get(login).equals(password)) {
            registered = true;
        }
        return registered;
    }

}