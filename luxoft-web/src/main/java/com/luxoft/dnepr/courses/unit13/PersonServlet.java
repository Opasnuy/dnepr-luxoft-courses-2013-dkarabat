package com.luxoft.dnepr.courses.unit13;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class PersonServlet extends HttpServlet {
   private static Map<String,String> map = new ConcurrentHashMap<>();



    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setCharacterEncoding("utf8");
        resp.setContentType("application/json");
        StringBuffer json = new StringBuffer();
        PrintWriter writer = resp.getWriter();


        String nameTmp = req.getParameter("name");
        String ageTmp = req.getParameter("age");
        if (nameTmp.equals("") || ageTmp.equals("")) {
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            json.append("{\"error\": \"Illegal parameters\"}");
        } else if (!nameTmp.equals("") && map.containsKey(nameTmp)) {
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            json.append("{\"error\": \"Name ");
            json.append(nameTmp + "already exists\"}");
        } else if (!nameTmp.equals("") && !ageTmp.equals("")){
           map.put(nameTmp,ageTmp);
            resp.setStatus(HttpServletResponse.SC_CREATED);
        }

        resp.setContentLength(json.length());
        writer.println(json);

    }


    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setCharacterEncoding("utf8");
        resp.setContentType("application/json");
        StringBuffer json = new StringBuffer();
        PrintWriter writer = resp.getWriter();
        String nameTmp = req.getParameter("name");
        String ageTmp = req.getParameter("age");
        if (nameTmp.equals("") || ageTmp.equals("")) {
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            json.append("{\"error\": \"Illegal parameters\"}");
        }else if (!nameTmp.equals("") && !map.containsKey(nameTmp)) {
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            json.append("{\"error\": \"Name ");
            json.append(nameTmp + " does not exist\"}");
        } else if (!nameTmp.equals("") && !ageTmp.equals("")){
            map.put(nameTmp,ageTmp);
            resp.setStatus(HttpServletResponse.SC_ACCEPTED);
        }

        resp.setContentLength(json.length());
        writer.println(json);

    }
}
