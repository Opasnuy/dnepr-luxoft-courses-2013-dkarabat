package com.luxoft.dnepr.courses.unit14.util;


public class PageBuilder {


    public static String getIndexPage(boolean registered) {
        String errorMessage = "";
        if (!registered) {
            errorMessage = "Wrong login or password";
        }
        return "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "    <title>Login Form</title>\n" +
                "    <meta charset=\"UTF-8\"/>\n" +
                "</head>\n" +
                "<body  bgcolor='#D0D0D0'>\n" +
                "<div align='center'>\n" +
                "    <form id=\"loginForm\" actoion=\"validate\" method=\"post\">\n" +
                "        <div style=\"font-size: 40px; font-weight: bolder;\">Login Page</div>\n" +
                "        <table>\n" +
                "            <tr>\n" +
                "               <td><label>User Name </label></td>\n" +
                "                  <td> <input type=\"text\"  name=\"log\"/></td>\n" +
                "                <td>\n" +"<div id=\"message\" style=\"color:red\">" + errorMessage + "</div>"+ "</td>\n" +
                "            </tr>\n" +
                "            <tr>\n" +
                "                <td><label >Password </label></td>\n" +
                "                <td> <input type=\"password\"  name=\"pwd\"/></td>\n" +
                "            </tr>\n" +
                "            <tr>\n" +
                "            <td></td>\n" +
                "            <td><input type=\"submit\"  value=\"Sign In\"  /></td>\n" +
                "            </tr>\n" +
                "            </table>\n" +
                "        </form></div>\n" +
                "</form>\n" +
                "</body>\n" +
                "</html>";
    }

    public static String getWelcomePage(String user) {
        return "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\"\n" +
                "        \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n" +
                "<html xmlns=\"http://www.w3.org/1999/xhtml\" xmlns=\"http://www.w3.org/1999/html\">\n" +
                "<head>\n" +
                "    <title>Login page</title>\n" +
                "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>\n" +
                "</head>\n" +
                "<body>\n" +
                "<div class=\"welcomeParent\" align='center'>\n" +
                "    <div class=\"welcomeContent welcomeText\">Hello " + user + "!</div>\n" +
                "    <div class=\"welcomeRight welcomeLogout\"><a href=\"logout\">logout</a></div>\n" +
                "</div>\n" +
                "</body>\n" +
                "</html>";
    }
}
