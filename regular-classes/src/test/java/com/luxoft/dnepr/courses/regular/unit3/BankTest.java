package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.NoUserFoundException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.TransactionException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class BankTest {

    private User user;
    private User user2;
    private Wallet wallet;
    private Wallet wallet2;
    private Map<Long, UserInterface> usersList = new HashMap<>();
    Bank bank;


    @Before
    public void prepare() {
        user = new User();
        user.setId(1L);
        user.setName("Dima");

        wallet = new Wallet();
        wallet.setId(1L);
        wallet.setAmount(new BigDecimal(10));
        wallet.setMaxAmount(new BigDecimal(15));
        user.setWallet(wallet);

        user2 = new User();
        user2.setId(2L);
        user2.setName("Vova");

        wallet2 = new Wallet();
        wallet2.setAmount(new BigDecimal(10));
        wallet2.setId(2L);
        wallet2.setMaxAmount(new BigDecimal(15));

        user2.setWallet(wallet2);

        usersList.put(user.getId(), user);
        usersList.put(user2.getId(), user2);
        bank = new Bank("1.7");
    }

    @After
    public void delete() {

    }

//    @Test
//    public void javaVersionTest() {
//        Bank bank = new Bank("1.7.0_08");
//    }

    @Test
    public void testGetUsers() throws Exception {
        bank.setUsers(usersList);
        Assert.assertTrue(bank.getUsers().equals(usersList));
    }

    @Test
    public void testSetUsers() throws Exception {
        bank.setUsers(usersList);
        Assert.assertEquals(user, usersList.get(1L));
        Assert.assertEquals(user2, usersList.get(2L));
    }

    @Test(expected = NoUserFoundException.class)
    public void testGetUserById() throws Exception {
        bank.setUsers(usersList);
        bank.getUserById(3L);
    }

    @Test(expected = TransactionException.class)
    public void testMakeMoneyTransactionBlockedFirstWallet() throws Exception {
        bank.setUsers(usersList);
        wallet.setStatus(WalletStatus.BLOCKED);
        wallet2.setStatus(WalletStatus.ACTIVE);
        bank.makeMoneyTransaction(1L, 2L, new BigDecimal(9));
    }

    @Test(expected = TransactionException.class)
    public void testMakeMoneyTransactionBlockedSecondWallet() throws Exception {
        bank.setUsers(usersList);
        wallet.setStatus(WalletStatus.ACTIVE);
        wallet2.setStatus(WalletStatus.BLOCKED);
        bank.makeMoneyTransaction(1L, 2L, new BigDecimal(9));
    }

    @Test(expected = TransactionException.class)
    public void testMakeMoneyTransactionInsufficient() throws Exception {
        bank.setUsers(usersList);
        wallet.setStatus(WalletStatus.ACTIVE);
        wallet2.setStatus(WalletStatus.ACTIVE);
        bank.makeMoneyTransaction(1L, 2L, new BigDecimal(11));
    }

    @Test(expected = TransactionException.class)
    public void testMakeMoneyTransactionLimit() throws Exception {
        bank.setUsers(usersList);
        wallet.setStatus(WalletStatus.ACTIVE);
        wallet2.setStatus(WalletStatus.ACTIVE);
        bank.makeMoneyTransaction(1L, 2L, new BigDecimal(10));
    }
}
