package com.luxoft.dnepr.courses.regular.unit7_1;

import org.junit.Assert;
import org.junit.Test;


public class ThreadProducerTest {



    @Test
    public void testGetNewThread() throws Exception {
        Assert.assertEquals(Thread.State.NEW, ThreadProducer.getNewThread().getState());

    }

    @Test
    public void testGetRunnableThread() throws Exception {
        Assert.assertEquals(Thread.State.RUNNABLE, ThreadProducer.getRunnableThread().getState());
    }

    @Test
    public void testGetBlockedThread() throws Exception {
        Assert.assertEquals(Thread.State.BLOCKED,ThreadProducer.getBlockedThread().getState());
    }

    @Test
    public void testGetWaitingThread() throws Exception {
        Assert.assertEquals(Thread.State.WAITING,ThreadProducer.getWaitingThread().getState());
    }

    @Test
    public void testGetTimedWaitingThread() throws Exception {
        Assert.assertEquals(Thread.State.TIMED_WAITING,ThreadProducer.getTimedWaitingThread().getState());
    }

    @Test
    public void testGetTerminatedThread() throws Exception {
        Assert.assertEquals(Thread.State.TERMINATED,ThreadProducer.getTerminatedThread().getState());
    }
}
