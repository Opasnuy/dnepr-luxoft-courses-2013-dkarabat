package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

/**
 *
 */
public class WalletTest {

    BigDecimal amount;
    WalletInterface wallet;
    BigDecimal amountToTransfer;

    @Before
    public void prepare() {
        amount = new BigDecimal(12).setScale(2, BigDecimal.ROUND_HALF_UP);
        amountToTransfer = new BigDecimal(6.30).setScale(2, BigDecimal.ROUND_HALF_UP);
        wallet = new Wallet();
        wallet.setAmount(new BigDecimal(10).setScale(2, BigDecimal.ROUND_HALF_UP));
        wallet.setMaxAmount(new BigDecimal(15).setScale(2, BigDecimal.ROUND_HALF_UP));
    }

    @Test(expected = WalletIsBlockedException.class)
    public void testCheckWithdrawalBlocked() throws Exception {
        wallet.setStatus(WalletStatus.BLOCKED);
        wallet.checkWithdrawal(amount);
    }

    @Test(expected = InsufficientWalletAmountException.class)
    public void testCheckWithdrawal() throws Exception {
        wallet.setStatus(WalletStatus.ACTIVE);
        wallet.checkWithdrawal(amount);
    }

    @Test()
    public void testWithdraw() throws Exception {
        BigDecimal ammountAfter = new BigDecimal(5.55).setScale(2, BigDecimal.ROUND_HALF_UP);
        wallet.withdraw(new BigDecimal(4.45));
        Assert.assertEquals(ammountAfter,wallet.getAmount() );
    }

    @Test(expected = WalletIsBlockedException.class)
    public void testCheckTransferBlocked() throws Exception {
        wallet.setStatus(WalletStatus.BLOCKED);
        wallet.checkTransfer(amountToTransfer);
    }

    @Test(expected = LimitExceededException.class)
    public void testCheckTransfer() throws Exception {
        wallet.setStatus(WalletStatus.ACTIVE);
        wallet.checkTransfer(amountToTransfer);
    }

    @Test
    public void testTransfer() throws Exception {
        BigDecimal amountAfterTransfer = new BigDecimal(16.30).setScale(2, BigDecimal.ROUND_HALF_UP);
        wallet.transfer(amountToTransfer);
        Assert.assertEquals(amountAfterTransfer, wallet.getAmount());
    }
}
