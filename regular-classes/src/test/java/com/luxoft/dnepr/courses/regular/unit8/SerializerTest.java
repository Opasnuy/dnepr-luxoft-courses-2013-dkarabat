package com.luxoft.dnepr.courses.regular.unit8;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Date;


public class SerializerTest {
    Person person;
    Person father;
    FamilyTree familyTree;
    File file = new File("C:\\tmp.txt");

    @Before
    public void prepare(){
        person = new Person();
        person.setBirthDate(new Date());
        person.setEthnicity("ukrainian");
        person.setGender(Gender.FEMALE);
        person.setName("Galja");
        father = new Person();
        father.setBirthDate(new Date());
        father.setEthnicity("ukrainian");
        father.setGender(Gender.MALE);
        father.setName("Vasiliy");
        person.setFather(father);
        familyTree = new FamilyTree(person);


    }

    @Test
    public void testSerialize() throws Exception {
        Serializer.serialize(file, familyTree);
    }

    @Test
    public void testDeserialize() throws Exception {
        FamilyTree ft= Serializer.deserialize(file);
        Assert.assertEquals("Galja", ft.getRoot().getName());
        Assert.assertEquals("ukrainian",ft.getRoot().getEthnicity());
        Assert.assertEquals("Vasiliy",ft.getRoot().getFather().getName());
        Assert.assertEquals("ukrainian",ft.getRoot().getFather().getEthnicity());
    }
}
