package com.luxoft.dnepr.courses.regular.unit4;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

/**
 *
 */
public class EqualSetTest {

    private EqualSet set;

    @Before
    public void prepare() {
        set = new EqualSet();
    }

    @Test
    public void testSize() throws Exception {
        set.add(1);
        set.add(2);
        set.add(3);
        Assert.assertEquals(3, set.size());
    }

    @Test
    public void testIsEmpty() throws Exception {
        Assert.assertTrue(set.isEmpty());
    }

    @Test
    public void testIsEmptyFalse() throws Exception {
        set.add(0);
        Assert.assertFalse(set.isEmpty());
    }


    @Test
    public void testContains() throws Exception {
        set.add(1);
        Assert.assertTrue(set.contains(1));
    }

    @Test
    public void testContainsFalse() throws Exception {
        set.add(1);
        Assert.assertFalse(set.contains(2));
    }

    @Test
    public void testToArray() throws Exception {
        Integer[] ints = {1, 2, 3};
        Assert.assertArrayEquals(ints, set.toArray(ints));
    }

    @Test
    public void testAdd() throws Exception {
        Assert.assertTrue(set.add(1));
        Assert.assertTrue(set.add(2));
    }

    @Test
    public void testAddFlse() throws Exception {
        Assert.assertTrue(set.add(1));
        Assert.assertFalse(set.add(1));
    }

    @Test
    public void testAddNull() throws Exception {
        Assert.assertTrue(set.add(null));
        Assert.assertFalse(set.add(null));
    }

    @Test
    public void testRemove() throws Exception {
        set.add(1);
        set.add(2);
        Assert.assertTrue(set.remove(2));

        Assert.assertFalse(set.contains(2));

    }

    @Test
    public void testContainsAll() throws Exception {
        ArrayList list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);

        set.add(1);
        set.add(2);
        set.add(3);

        Assert.assertTrue(set.containsAll(list));
    }

    @Test
    public void testAddAll() throws Exception {
        ArrayList list = new ArrayList<>();
        list.add(1);
        list.add(2);
        set.addAll(list);
        Assert.assertEquals(2, set.size());
        Assert.assertTrue(set.contains(1));
        Assert.assertTrue(set.contains(2));

    }

    @Test
    public void testAddAllFalse() throws Exception {
        ArrayList list = new ArrayList<>();
        list.add(1);
        list.add(1);
        list.add(1);
        set.addAll(list);
        Assert.assertEquals(1,set.size());
    }

    @Test
    public void testRetainAll() throws Exception {
        ArrayList list = new ArrayList<>();
        list.add(2);
        list.add(4);

        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);

        set.retainAll(list);
        Assert.assertEquals(2, set.size());
        Assert.assertFalse(set.contains(1));
        Assert.assertFalse(set.contains(3));
    }

    @Test
    public void testRemoveAll() throws Exception {
        ArrayList list = new ArrayList<>();
        list.add(1);
        list.add(2);

        set.add(1);
        set.add(2);
        Assert.assertTrue(set.removeAll(list));
        Assert.assertTrue(set.isEmpty());
    }

    @Test
    public void testClear() throws Exception {
        set.add(1);
        set.add(2);
        set.clear();
        Assert.assertTrue(set.isEmpty());

    }
}
