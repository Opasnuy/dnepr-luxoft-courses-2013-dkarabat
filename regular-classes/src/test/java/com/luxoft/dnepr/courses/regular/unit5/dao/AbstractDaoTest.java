package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.exeption.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exeption.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Employee;
import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AbstractDaoTest {
    private Employee employee = new Employee();
    private Redis redis = new Redis();
    IDao<Employee>  employeeDao = new EmployeeDaoImpl();
    IDao<Redis> redisDao = new RedisDaoImpl();

    @Before
    public void prepare(){
        employee.setId(1L);
        employee.setSalary(1000);

        redis.setId(2L);
        redis.setWeight(50);

    }

    @Test
    public void testSaveIdNull() throws Exception, UserAlreadyExist {
        EntityStorage.getEntities().put(employee.getId(),employee);
        Employee emp = new Employee();
        emp.setSalary(300);
        employeeDao.save(emp);
        Assert.assertTrue(EntityStorage.getEntities().containsKey(2L));
    }

    @Test(expected = UserAlreadyExist.class)
    public void testSaveIdCopy() throws Exception, UserAlreadyExist {
        EntityStorage.getEntities().put(employee.getId(),employee);
        Employee emp = new Employee();
        emp.setId(1L);
        emp.setSalary(300);
        employeeDao.save(emp);
    }


    @Test
    public void testUpdate() throws Exception, UserNotFound {
        Redis redisNew = new Redis();
        redisNew.setId(2L);
        redisNew.setWeight(30);

        EntityStorage.getEntities().put(redis.getId(),redis);
        redisDao.update(redisNew);
        Assert.assertEquals(30, redisDao.get(2).getWeight());

    }

    @Test(expected = UserNotFound.class)
    public void testUpdateWrongId() throws Exception, UserNotFound {
        Redis redisNew = new Redis();
        redisNew.setId(4L);
        redisNew.setWeight(30);

        EntityStorage.getEntities().put(redis.getId(),redis);
        redisDao.update(redisNew);
    }

    @Test(expected = UserNotFound.class)
    public void testUpdateNullId() throws Exception, UserNotFound {
        Redis redisNew = new Redis();
        redisNew.setWeight(30);

        EntityStorage.getEntities().put(redis.getId(),redis);
        redisDao.update(redisNew);
    }

    @Test
    public void testGet() throws Exception {
        Assert.assertEquals(EntityStorage.getEntities().get(1),employeeDao.get(1));
    }

    @Test
    public void testGetNull() throws Exception {
        Assert.assertEquals(null,employeeDao.get(5));

    }

    @Test
    public void testDelete() throws Exception {
        Assert.assertTrue(employeeDao.delete(1L));
        Assert.assertEquals(2,EntityStorage.getEntities().size());
    }
}
