package com.luxoft.dnepr.courses.regular.unit8;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Person implements Externalizable {

    private static final String DATA_FORMAT = "EEE MMM dd HH:mm:ss zzz yyyy";

    private String name;
    private Gender gender;
    private String ethnicity;
    private Date birthDate;
    private Person father;
    private Person mother;

    public Person() {

    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        in.readUTF();
        name = in.readUTF();
        in.readUTF();
        in.readUTF();
        gender = Gender.valueOf(in.readUTF());
        in.readUTF();
        in.readUTF();
        ethnicity = in.readUTF();
        in.readUTF();
        in.readUTF();
        Date date = null;
        String stringDate = in.readUTF();
        SimpleDateFormat format = new SimpleDateFormat(DATA_FORMAT, Locale.ENGLISH);
        try {
            date = format.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        birthDate = date;
        in.readUTF();
        String tmp = in.readUTF();
        if (tmp.equals("father :")) {
            father = new Person();
            father.readExternal(in);
        }
        if (tmp.equals("mother :")) {
            mother = new Person();
            mother.readExternal(in);
        }
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeUTF("{\"name\":\"");
        out.writeUTF(this.name);
        out.writeUTF("\",");
        out.writeUTF("\"gender\":\"");
        out.writeUTF(this.gender.toString());
        out.writeUTF("\",");
        out.writeUTF("\"ethnicity\":\"");
        out.writeUTF(this.ethnicity);
        out.writeUTF("\",");
        SimpleDateFormat sdfSource = new SimpleDateFormat(DATA_FORMAT,Locale.ENGLISH);
        out.writeUTF("\"birthDate\":\"");
        try {
            out.writeUTF(String.valueOf(sdfSource.parse(birthDate.toString())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        out.writeUTF("\",");
        if (father != null) {
            out.writeUTF("father :");
            father.writeExternal(out);
        }
        if ((mother != null)) {
            out.writeUTF("mother :");
            mother.writeExternal(out);
        }

        out.writeUTF("}");

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Person getFather() {
        return father;
    }

    public void setFather(Person father) {
        this.father = father;
    }

    public Person getMother() {
        return mother;
    }

    public void setMother(Person mother) {
        this.mother = mother;
    }
}
