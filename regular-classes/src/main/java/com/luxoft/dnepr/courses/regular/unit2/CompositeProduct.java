package com.luxoft.dnepr.courses.regular.unit2;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents group of similar {@link Product}s.
 * Implementation of pattern Composite.
 */
public class CompositeProduct implements Product {
    private List<Product> childProducts = new ArrayList<Product>();

    /**
     * Returns code of the first "child" product or null, if child list is empty
     *
     * @return product code
     */
    @Override
    public String getCode() {
        if (!childProducts.isEmpty()) {
            return childProducts.get(0).getCode();
        } else {
            return null;
        }
    }

    /**
     * Returns name of the first "child" product or null, if child list is empty
     *
     * @return product name
     */
    @Override
    public String getName() {
        if (!childProducts.isEmpty()) {
            return childProducts.get(0).getName();
        } else {
            return null;
        }
    }

    /**
     * Returns total price of all the child products taking into account discount.
     * 1 item - no discount
     * 2 items - 5% discount
     * >= 3 items - 10% discount
     *
     * @return total price of child products
     */
    @Override
    public double getPrice() {
        double price = 0;
        if (childProducts.size() == 2) {
            price = totalPrice() * 0.95;
        } else if (childProducts.size() >= 3) {
            price = totalPrice() * 0.90;
        }
        else {
            price = totalPrice();
        }
        return price;
    }

    private double totalPrice() {
        double price = 0;
        for (Product product : childProducts) {
            price += product.getPrice();
        }
        return price;
    }

    public int getAmount() {
        return childProducts.size();
    }

    public void add(Product product) {
        childProducts.add(product);
    }

    public void remove(Product product) {
        childProducts.remove(product);
    }

    public List<Product> getChildProducts(){
        return childProducts;
    }

    public Product getProduct(){
        return childProducts.get(0);
    }

    @Override
    public String toString() {
        return getName() + " * " + getAmount() + " = " + getPrice();
    }
}
