package com.luxoft.dnepr.courses.regular.unit6.dao;


import com.luxoft.dnepr.courses.regular.unit6.exeptions.EntityAlreadyExistException;
import com.luxoft.dnepr.courses.regular.unit6.exeptions.EntityNotFoundException;
import com.luxoft.dnepr.courses.regular.unit6.model.Entity;
import com.luxoft.dnepr.courses.regular.unit6.storage.EntityStorage;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public abstract class AbstractDao implements IDao {

    private final ReentrantReadWriteLock fLock = new ReentrantReadWriteLock();
    private final Lock fWriteLock = fLock.writeLock();

    @Override
    public synchronized Entity save(Entity entity) throws EntityAlreadyExistException {
        if (entity.getId() == null) {
            entity.setId(EntityStorage.getNextId());
        }
        if (EntityStorage.getEntities().containsKey(entity.getId())) {
            throw new EntityAlreadyExistException("This user already saved");
        }
            EntityStorage.getEntities().put(entity.getId(), entity);
        return entity;
    }

    @Override
    public synchronized Entity update(Entity entity) throws EntityNotFoundException {
        if (entity.getId() == null || !EntityStorage.getEntities().containsKey(entity.getId())) {
            throw new EntityNotFoundException("Not found user");
        }

            EntityStorage.getEntities().put(entity.getId(), entity);

        return entity;
    }

    @Override
    public  Entity get(long id) {
        if (!EntityStorage.getEntities().containsKey(id)) {
            return null;
        }
        return EntityStorage.getEntities().get(id);
    }

    @Override
    public synchronized boolean delete(long id) {
        boolean delete = false;

        if (EntityStorage.getEntities().containsKey(id)) {
            EntityStorage.getEntities().remove(EntityStorage.getEntities().remove(id));
            delete = true;
        }
        return delete;
    }
}
