package com.luxoft.dnepr.courses.regular.unit18.logic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Random;
import java.util.StringTokenizer;

public class Vocabulary {

    private HashSet<String> vocabulary = new HashSet();

    public Vocabulary() {
        addWordsToVocabulary(readBook());
    }

    public String readBook() {
        StringBuilder sb = new StringBuilder();
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(Vocabulary.class.getResourceAsStream("/unit18/sonnets.txt")))) {
            String inputLine;
            while ((inputLine = br.readLine()) != null) {
                sb.append(inputLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }


    public HashSet<String> getVocabulary() {
        return vocabulary;
    }

    public void addWordsToVocabulary(String input) {
        StringTokenizer t = new StringTokenizer(input.toString());
        while (t.hasMoreTokens()) {
            String word = t.nextToken();
            if (word.length() > 3) {
                splitAndAddWordsToVocabulary(word);
            }
        }
    }

    private void splitAndAddWordsToVocabulary(String input) {
        String tmp = input.replace(",", " ").replace(";", " ").replace(":", " ").replace(".", " ");
        String[] words = tmp.split(" ");
        for (String wd : words) {
            vocabulary.add(wd.toLowerCase());
        }
    }


    public String grtRandomWord() {
        String randomWord = "";
        int item = new Random().nextInt(vocabulary.size());
        int i = 0;
        for (Object obj : vocabulary) {
            if (i == item) {
                randomWord = (String) obj;
            }
            i++;
        }
        return randomWord;
    }

}
