package com.luxoft.dnepr.courses.regular.unit2;

public class Beverage extends AbstractProduct {

    private boolean nonAlcoholic;

    public boolean isNonAlcoholic() {
        return nonAlcoholic;
    }

    public void setNonAlcoholic(boolean alcoholic) {
        this.nonAlcoholic = alcoholic;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if ((o == null) || !(o instanceof Beverage)) return false;
        if (!super.equals(o)) return false;

        Beverage beverage = (Beverage) o;

        if (nonAlcoholic != beverage.nonAlcoholic) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return (nonAlcoholic ? 1 : 0);
    }

    public Object clone() throws CloneNotSupportedException {
        return (Beverage)super.clone();
    }
}
