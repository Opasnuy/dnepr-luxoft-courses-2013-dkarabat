package com.luxoft.dnepr.courses.regular.unit18.util;


public class Constants {
    public static final String PROPOSITION = "Do you know translation of this word?:";
    public static final String HELP_MESSAGE = "Linguistic analyzer v1, actor Tushar Brahmacobalol, for help type -" +
            " help, answer y/n on question, your english knowledge will show your estimated vocabulary ";
    public static final String ERROR_MESSAGE = "Error while reading answer";
}
