package com.luxoft.dnepr.courses.regular.unit6.storage;

import com.luxoft.dnepr.courses.regular.unit6.model.Entity;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 */
public class EntityStorage {

    private final static Map<Long, Entity> entities = new ConcurrentHashMap();
    public final static Entity NULL_VALUE = new Entity();

    private EntityStorage() {}

    public synchronized static Map<Long, Entity> getEntities() {
        return entities;
    }

    public synchronized static Long getNextId() {
        long id = 0;
        id = (entities.size() == 0)?1L: Collections.max(entities.keySet()) + 1;
        entities.put(id + 1, NULL_VALUE);
        return id + 1;
    }

}
