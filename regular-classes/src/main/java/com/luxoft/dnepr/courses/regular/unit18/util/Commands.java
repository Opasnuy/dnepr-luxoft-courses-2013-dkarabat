package com.luxoft.dnepr.courses.regular.unit18.util;


public enum Commands {
    EXIT("exit"),
    HELP("help"),
    YES("y"),
    NO("n"),
    ERROR("");

    private String typeValue;

    private Commands(String type) {
        typeValue = type;
    }

    static public Commands getType(String pType) {
        for (Commands type: Commands.values()) {
            if (type.getTypeValue().equals(pType)) {
                return type;
            }
        }
        return ERROR;
    }

    public String getTypeValue() {
        return typeValue;
    }

}
