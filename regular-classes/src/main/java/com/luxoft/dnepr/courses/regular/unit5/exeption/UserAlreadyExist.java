package com.luxoft.dnepr.courses.regular.unit5.exeption;

/**
 *
 */
public class UserAlreadyExist extends Throwable {

    public UserAlreadyExist() {
        super();
    }

    public UserAlreadyExist(String message, Throwable cause) {
        super(message, cause);
    }

    public UserAlreadyExist(String message) {
        super(message);
    }

    public UserAlreadyExist(Throwable cause) {
        super(cause);
    }
}
