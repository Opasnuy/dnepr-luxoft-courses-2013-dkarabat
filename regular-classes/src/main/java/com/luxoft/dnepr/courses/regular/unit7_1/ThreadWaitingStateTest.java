package com.luxoft.dnepr.courses.regular.unit7_1;

public class ThreadWaitingStateTest extends Thread {
    public void run() {
        try {
            synchronized (this) {
                wait();
            }
        } catch (InterruptedException e) {
            System.err.print("ошибка потока");
        }
    }
}
