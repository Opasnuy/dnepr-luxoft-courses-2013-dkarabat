package com.luxoft.dnepr.courses.regular.unit6.dao;

import com.luxoft.dnepr.courses.regular.unit6.exeptions.EntityAlreadyExistException;
import com.luxoft.dnepr.courses.regular.unit6.exeptions.EntityNotFoundException;
import com.luxoft.dnepr.courses.regular.unit6.model.Entity;

/**
 *
 */
public interface IDao<E extends Entity> {
    E save(E e) throws EntityAlreadyExistException;
    E update(E e) throws EntityNotFoundException;
    E get(long id);
    boolean delete(long id);
}
