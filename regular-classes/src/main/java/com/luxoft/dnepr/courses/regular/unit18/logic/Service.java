package com.luxoft.dnepr.courses.regular.unit18.logic;


import com.luxoft.dnepr.courses.regular.unit18.util.Commands;
import com.luxoft.dnepr.courses.regular.unit18.util.Constants;

import java.util.Scanner;

public class Service {

    public void view() {
        Vocabulary vocabulary = new Vocabulary();
        String word = vocabulary.grtRandomWord();
        System.out.println(Constants.PROPOSITION + "\n" + word);
        Scanner input = new Scanner(System.in);
        String inputCommand = input.next();
        doCommand(inputCommand, word, vocabulary);
    }

    private void doCommand(String inputCommand, String word, Vocabulary vocabulary) {
        Commands command = Commands.getType(inputCommand.toLowerCase());
        switch (command) {
            case EXIT:
                System.out.println("Your estimated vocabulary is " + Words.getResult(vocabulary.getVocabulary().size())
                        + " words");
                System.exit(0);
            case HELP:
                System.out.println(Constants.HELP_MESSAGE);
                break;
            case YES:
                Words.addAnswer(word, true);
                break;
            case NO:
                Words.addAnswer(word, false);
                break;
            case ERROR:
                System.out.println(Constants.ERROR_MESSAGE);
                break;
        }
    }
}
