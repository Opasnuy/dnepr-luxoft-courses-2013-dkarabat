package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;

import java.math.BigDecimal;

/**
 *
 */
public class Wallet implements WalletInterface {

    private Long walletId;
    private BigDecimal amount;
    private BigDecimal maxAmount;
    private WalletStatus walletStatus;


    @Override
    public Long getId() {
        return walletId;
    }

    @Override
    public void setId(Long id) {
        this.walletId = id;
    }

    @Override
    public BigDecimal getAmount() {
        return amount.setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    @Override
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public WalletStatus getStatus() {
        return walletStatus;
    }

    @Override
    public void setStatus(WalletStatus status) {
        this.walletStatus = status;
    }

    @Override
    public BigDecimal getMaxAmount() {
        return maxAmount.setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    @Override
    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    @Override
    public void checkWithdrawal(BigDecimal amountToWithdraw) throws WalletIsBlockedException, InsufficientWalletAmountException {
        if (getStatus().equals(WalletStatus.BLOCKED)) {
            throw new WalletIsBlockedException(getId(), " wallet is blocked");
        }
        if (amountToWithdraw.compareTo(getAmount()) == 1) {
            throw new InsufficientWalletAmountException(getId(), amountToWithdraw, getAmount(),
                    " insufficient funds ");
        }
    }

    @Override
    public void withdraw(BigDecimal amountToWithdraw) {
        setAmount(getAmount().subtract(amountToWithdraw).setScale(2, BigDecimal.ROUND_HALF_UP));
    }

    @Override
    public void checkTransfer(BigDecimal amountToTransfer) throws WalletIsBlockedException, LimitExceededException {
        if (getStatus().equals(WalletStatus.BLOCKED)) {
            throw new WalletIsBlockedException(getId(), " wallet is blocked");
        }

        if ((amountToTransfer.add(getAmount().setScale(2, BigDecimal.ROUND_HALF_UP)).compareTo(getMaxAmount()) == 1)) {
            throw new LimitExceededException(getId(), amountToTransfer, getMaxAmount(),
                    " wallet limit exceeded ");
        }
    }

    @Override
    public void transfer(BigDecimal amountToTransfer) {
        setAmount(getAmount().add(amountToTransfer).setScale(2, BigDecimal.ROUND_HALF_UP));
    }
}
