package com.luxoft.dnepr.courses.regular.unit5.dao;


import com.luxoft.dnepr.courses.regular.unit5.exeption.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exeption.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;

import java.util.Collections;

public abstract class AbstractDao implements IDao {
    @Override
    public Entity save(Entity entity) throws UserAlreadyExist {
        if (entity.getId() == null) {
            if (EntityStorage.getEntities().isEmpty()) {
                entity.setId(1L);
            } else {
                entity.setId(Collections.max(EntityStorage.getEntities().keySet()) + 1);
            }
        }
        if (EntityStorage.getEntities().containsKey(entity.getId())) {
            throw new UserAlreadyExist("This user already saved");
        }
        EntityStorage.getEntities().put(entity.getId(), entity);
        return entity;
    }

    @Override
    public Entity update(Entity entity) throws UserNotFound {
        if (entity.getId() == null || !EntityStorage.getEntities().containsKey(entity.getId())) {
            throw new UserNotFound("Not found user");
        }
        EntityStorage.getEntities().put(entity.getId(), entity);
        return entity;
    }

    @Override
    public Entity get(long id) {
        if (!EntityStorage.getEntities().containsKey(id)) {
            return null;
        }
        return EntityStorage.getEntities().get(id);
    }

    @Override
    public boolean delete(long id) {
        boolean delete = false;

       if( EntityStorage.getEntities().containsKey(id)){
            EntityStorage.getEntities().remove(EntityStorage.getEntities().remove(id));
           delete = true;
       }
        return delete;
    }
}
