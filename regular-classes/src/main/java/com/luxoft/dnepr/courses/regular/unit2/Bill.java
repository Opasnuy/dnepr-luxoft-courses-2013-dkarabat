package com.luxoft.dnepr.courses.regular.unit2;

import java.util.*;

/**
 * Represents a bill.
 * Combines equal {@link Product}s and provides information about total price.
 */
public class Bill {

    private ArrayList<CompositeProduct> products = new ArrayList<CompositeProduct>();


    /**
     * Appends new instance of product into the bill.
     * Groups all equal products using {@link CompositeProduct}
     *
     * @param product new product
     */
    public void append(Product product) {
        if (products.isEmpty() || !addProduct(product)) {
            CompositeProduct cp = new CompositeProduct();
            cp.add(product);
            products.add(cp);
        }
    }

    private boolean addProduct(Product product) {
        boolean entry = false;
        if (!products.isEmpty()) {
            for (CompositeProduct compositeProduct : products) {
                if (compositeProduct.getChildProducts().isEmpty()) {
                    compositeProduct.add(product);
                    entry = true;
                } else if (compositeProduct.getProduct() != null) {
                    if (compositeProduct.getProduct().equals(product)) {
                        compositeProduct.add(product);
                        entry = true;
                    }
                }
            }
        }
        return entry;
    }


    /**
     * Calculates total cost of all the products in the bill including discounts.
     *
     * @return
     */
    public double summarize() {
        double cost = 0;

        for (CompositeProduct compositeProduct : products) {
            cost += compositeProduct.getPrice();
        }

        return cost;
    }

    /**
     * Returns ordered list of products, all equal products are represented by single element in this list.
     * Elements should be sorted by their price in descending order.
     * See {@link CompositeProduct}
     *
     * @return
     */
    public List<Product> getProducts() {
        ArrayList<Product> bill = new ArrayList<>();
        Collections.sort(products, new ProductComparator());

        for (CompositeProduct compositeProduct : products) {
            AbstractProduct product = (AbstractProduct) compositeProduct.getProduct();
            product.setPrice(compositeProduct.getPrice());
            bill.add(product);
        }

        return bill;
    }

    private static class ProductComparator implements Comparator<Product> {

        @Override
        public int compare(Product o1, Product o2) {
            if (o1.getPrice() == o2.getPrice()) return 0;
            if (o1.getPrice() < o2.getPrice()) return 1;
            else return -1;
        }
    }

    @Override
    public String toString() {
        List<String> productInfos = new ArrayList<String>();
        for (Product product : getProducts()) {
            productInfos.add(product.toString());
        }
        return productInfos.toString() + "\nTotal cost: " + summarize();
    }
}
