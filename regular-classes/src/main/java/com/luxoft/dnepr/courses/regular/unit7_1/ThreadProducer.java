package com.luxoft.dnepr.courses.regular.unit7_1;


public class ThreadProducer {
    ThreadProducer() {
    }

    public static Thread getNewThread() {
        Thread thread = new ThreadTimedWaitingStateTest();
        return thread;
    }

    public static Thread getRunnableThread() {
        Thread t = new Thread(new Runnable() {
            public void run() {
                while (true) {

                }
            }
        });
        t.start();
        return t;
    }

    public static Thread getBlockedThread() {
        Thread thread = new ThreadWaitingStateTest();
        thread.start();
        synchronized (thread) {

            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return thread;
    }


    public static Thread getWaitingThread() {
        Thread thread = new ThreadWaitingStateTest();
        try {
            thread.start();
            synchronized (thread) {
                Thread.sleep(10);
            }
            Thread.sleep(10);
            thread.interrupt();
        } catch (InterruptedException e) {
            System.err.print("ошибка потока");
        }
        return thread;
    }

    public static Thread getTimedWaitingThread() {
        Thread thread = new ThreadTimedWaitingStateTest();
        thread.start();
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return thread;
    }

    public static Thread getTerminatedThread() {
        Thread t = null;
        try {
            t = new Thread();
            t.start();
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return t;
    }
}

