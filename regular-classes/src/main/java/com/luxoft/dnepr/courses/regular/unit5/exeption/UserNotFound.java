package com.luxoft.dnepr.courses.regular.unit5.exeption;

/**
 *
 */
public class UserNotFound extends Throwable {

    public UserNotFound() {
        super();
    }

    public UserNotFound(String message, Throwable cause) {
        super(message, cause);
    }

    public UserNotFound(String message) {
        super(message);
    }

    public UserNotFound(Throwable cause) {
        super(cause);
    }
}
