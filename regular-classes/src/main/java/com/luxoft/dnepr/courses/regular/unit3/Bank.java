package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class Bank implements BankInterface {

    private Map<Long, UserInterface> users = new HashMap<>();

    public Bank(String expectedJavaVersion) {
        String javaVersion = System.getProperty("java.version");
        if (!javaVersion.startsWith(expectedJavaVersion)) {
            throw new IllegalJavaVersionError(javaVersion, expectedJavaVersion, "");
        }
    }

    @Override
    public Map<Long, UserInterface> getUsers() {
        return users;
    }

    @Override
    public void setUsers(Map<Long, UserInterface> users) {
        this.users = users;
    }

    public UserInterface getUserById(Long id) throws NoUserFoundException {
        if (users.get(id) == null) {
            throw new NoUserFoundException(id, "No such user with id " + id);
        }
        return users.get(id);
    }


    @Override
    public void makeMoneyTransaction(Long fromUserId, Long toUserId, BigDecimal amount) throws NoUserFoundException, TransactionException {
        try {

            WalletInterface walletWithdraw = getUserById(fromUserId).getWallet();
            WalletInterface walletTransfer = getUserById(toUserId).getWallet();
            walletWithdraw.checkWithdrawal(amount);
            walletTransfer.checkTransfer(amount);
            walletWithdraw.withdraw(amount);
            walletTransfer.transfer(amount);

        } catch (WalletIsBlockedException e) {
            throw new TransactionException("User "
                    + "'" + users.get(e.getWalletId()).getName() + "'"
                    + " wallet is blocked");
        } catch (InsufficientWalletAmountException e) {
            throw new TransactionException("User "
                    + "'" + users.get(fromUserId).getName() + "'"
                    + " has insufficient funds ("
                    + e.getAmountInWallet()
                    + " < " + e.getAmountToWithdraw() + ")");
        } catch (LimitExceededException e) {
            throw new TransactionException("User "
                    + "'" + users.get(toUserId).getName() + "'"
                    + " wallet limit exceeded ("
                    + e.getAmountInWallet() + " + "
                    + e.getAmountToTransfer() + " > " + e.getAmountInWallet() + ")");
        }
    }
}
