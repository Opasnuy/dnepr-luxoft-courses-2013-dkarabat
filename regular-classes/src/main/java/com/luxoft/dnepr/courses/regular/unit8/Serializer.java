package com.luxoft.dnepr.courses.regular.unit8;

import java.io.*;
import java.util.Date;

public class Serializer {

    public static void serialize(File file, FamilyTree entity) throws IOException {
        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(entity);
        oos.flush();
        oos.close();
    }

    public static FamilyTree deserialize(File file) throws IOException, ClassNotFoundException {
        FamilyTree userRead;
        FileInputStream fis = new FileInputStream(file);
        ObjectInputStream ois = new ObjectInputStream(fis);
        userRead = (FamilyTree) ois.readObject();
        return userRead;
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Person person = new Person();
        person.setBirthDate(new Date());
        person.setEthnicity("ukrainian");
        person.setGender(Gender.FEMALE);
        person.setName("Galja");
        Person father = new Person();
        father.setBirthDate(new Date());
        father.setEthnicity("ukrainian");
        father.setGender(Gender.MALE);
        father.setName("Vasiliy");
        person.setFather(father);
        FamilyTree obj = new FamilyTree(person);

        File file = new File("E:\\t.txt");
        serialize(file, obj);
       FamilyTree ft= deserialize(file);
        System.out.println(ft.getRoot().getFather().getBirthDate());

    }
}

