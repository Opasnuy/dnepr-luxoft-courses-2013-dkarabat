package com.luxoft.dnepr.courses.regular.unit18.logic;

import java.util.ArrayList;
import java.util.List;

public class Words {

    public static List<String> knownWords = new ArrayList();
    public static List<String> unKnownWords = new ArrayList();

    public static int getResult(int total) {
        return total * (knownWords.size() + 1) / (knownWords.size() + unKnownWords.size() + 1);
    }

    public static void addAnswer(String word, Boolean know) {
        if (know) {
            knownWords.add(word);
        } else {
            unKnownWords.add(word);
        }
    }

}
