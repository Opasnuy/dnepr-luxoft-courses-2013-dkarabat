package com.luxoft.dnepr.courses.compiler;

import java.io.ByteArrayOutputStream;
import java.util.*;

public class Compiler {

    public static final boolean DEBUG = true;

    public static void main(String[] args) {
        byte[] byteCode = compile(getInputString(args));
        VirtualMachine vm = VirtualMachineEmulator.create(byteCode, System.out);
        vm.run();
    }

    static byte[] compile(String input) {
        ByteArrayOutputStream result = null;
        input.replaceAll("( )+", " ");
        if (Parser.checkParentheses(input)) {
            System.out.println(input);
            input.replace(" ", "");
            result = eval(input);
        }
        else {
            throw new CompilationException("Incorrect expression");
        }

        addCommand(result, VirtualMachine.PRINT);
        return result.toByteArray();
    }

    public static ByteArrayOutputStream eval(String input) {

        String[] tmp = convertInfixToPOstfix(Parser.addSpaces(input)).split(" ");
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        for (int i = 0; i < tmp.length; i++) {
            if (Parser.isOperand(tmp[i])) {
                addCommand(result, VirtualMachine.PUSH, Double.parseDouble(tmp[i]));
            } else if (tmp[i].equals("+")) {
                addCommand(result, VirtualMachine.ADD);
            } else if (tmp[i].equals("-")) {
                addCommand(result, VirtualMachine.SWAP);
                addCommand(result, VirtualMachine.SUB);
            } else if (tmp[i].equals("*")) {
                addCommand(result, VirtualMachine.MUL);
            } else if (tmp[i].equals("/")) {
                addCommand(result, VirtualMachine.SWAP);
                addCommand(result, VirtualMachine.DIV);
            } else if (tmp[i].equals(" ")) {
                continue;
            }
        }
        return result;
    }

    /**
     * Converts infix expression to postfix expression.
     *
     * @param infix
     * @return
     */
    public static String convertInfixToPOstfix(String infix) {
        Stack<String> stack = new Stack<String>();
        String postfix = "";
        String space = " ";
        StringTokenizer st = new StringTokenizer(infix);
        while (st.hasMoreTokens()) {
            String token = st.nextToken();
            if (Parser.isOperand(token)) {
                postfix += token + space;
            } else if (token.equals("(")) {
                stack.push(token);
            } else if (Parser.isOperator(token)) {
                while (!stack.empty() && Parser.getPriority(stack.peek()) >=
                        Parser.getPriority(token)) {
                    postfix += stack.pop() + space;
                }
                stack.push(token);
            } else if (token.equals(")")) {
                while (!stack.peek().equals("(")) {
                    postfix += stack.pop() + space;
                }
                stack.pop();
            }
        }
        while (!stack.empty()) {
            postfix += stack.pop() + space;
        }
        return postfix;
    }

    /**
     * Adds specific command to the byte stream.
     *
     * @param result
     * @param command
     */
    public static void addCommand(ByteArrayOutputStream result, byte command) {
        result.write(command);
    }

    /**
     * Adds specific command with double parameter to the byte stream.
     *
     * @param result
     * @param command
     * @param value
     */
    public static void addCommand(ByteArrayOutputStream result, byte command, double value) {
        result.write(command);
        writeDouble(result, value);
    }

    private static void writeDouble(ByteArrayOutputStream result, double val) {
        long bits = Double.doubleToLongBits(val);

        result.write((byte) (bits >>> 56));
        result.write((byte) (bits >>> 48));
        result.write((byte) (bits >>> 40));
        result.write((byte) (bits >>> 32));
        result.write((byte) (bits >>> 24));
        result.write((byte) (bits >>> 16));
        result.write((byte) (bits >>> 8));
        result.write((byte) (bits >>> 0));
    }

    private static String getInputString(String[] args) {
        if (args.length > 0) {
            return join(Arrays.asList(args));
        }

        Scanner scanner = new Scanner(System.in);
        List<String> data = new ArrayList<String>();
        while (scanner.hasNext()) {
            data.add(scanner.next());
        }
        return join(data);
    }

    private static String join(List<String> list) {
        StringBuilder result = new StringBuilder();
        for (String element : list) {
            result.append(element);
        }
        return result.toString();
    }


}
