package com.luxoft.dnepr.courses.regular.unit18.view;

import com.luxoft.dnepr.courses.regular.unit18.logic.Service;


public class UI implements Runnable {

    public void run() {
        Service service = new Service();
        while (true) {
            service.view();
        }
    }


    public static void main(String[] args) {
        UI ui = new UI();
        ui.run();
    }
}
