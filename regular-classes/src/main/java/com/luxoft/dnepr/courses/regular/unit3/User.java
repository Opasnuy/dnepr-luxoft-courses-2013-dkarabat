package com.luxoft.dnepr.courses.regular.unit3;

/**
 *
 */
public class User implements UserInterface {

    private Long userId;
    private String name;
    private WalletInterface walletInterface;


    @Override
    public Long getId() {
        return userId;
    }

    @Override
    public void setId(Long id) {
        this.userId = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public WalletInterface getWallet() {
        return walletInterface;
    }

    @Override
    public void setWallet(WalletInterface wallet) {
        this.walletInterface = wallet;
    }
}
