package com.luxoft.dnepr.courses.compiler;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 */
public class Parser {

    public static final int LOW_PRIORITY = 1;
    public static final int HIGH_PRIORITY = 2;

    /**
     *
     * @param s
     * @return
     */
    public static boolean isOperator(String s) {
        String operatorList = "+-*/";
        return operatorList.indexOf(s) >= 0;
    }

    /**
     *
     * @param s
     * @return
     */
    public static boolean checkParentheses(String s) {
        int nesting = 0;
        for (int i = 0; i < s.length(); ++i)
        {
            char c = s.charAt(i);
            switch (c) {
                case '(':
                    nesting++;
                    break;
                case ')':
                    nesting--;
                    if (nesting < 0) {
                        return false;
                    }
                    break;
            }
        }
        return nesting == 0;
    }

    /**
     *
     * @param source
     * @return
     */
    public static String addSpaces(String source) {
        StringBuilder result = new StringBuilder();
        Pattern p = Pattern.compile("([\\d]+[.]?+([\\d]+)?)|[\\(\\)\\+\\-\\*\\/]");
        Matcher m = p.matcher(source);
        while (m.find()) {
            result.append(source.substring(m.start(), m.end()));
            if (m.end() < source.length()) result.append(" ");
        }
        return result.toString();

    }

    /**
     *
     * @param operand
     * @return
     */
    public static boolean isOperand(String operand) {
        try {
            Double.parseDouble(operand);
        } catch (Exception ignore) {
            return false;
        }
        return true;
    }

    /**
     *
     * @param operator
     * @return
     */
    public static int getPriority(String operator) {
        int priority = 0;
        switch (operator) {

            case "+":
                priority = LOW_PRIORITY;
            break;
            case "-":
               priority = LOW_PRIORITY;
            break;
            case "*":
                priority = HIGH_PRIORITY;
            break;
            case "/":
               priority = HIGH_PRIORITY;
            break;
            default:
                return 0;
        }
        return priority;
    }


}
