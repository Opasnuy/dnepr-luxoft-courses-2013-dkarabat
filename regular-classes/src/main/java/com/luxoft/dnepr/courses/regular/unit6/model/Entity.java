package com.luxoft.dnepr.courses.regular.unit6.model;

/**
 *
 */
public class Entity {

    public Entity(Long id) {
        this.id = id;
    }

    private Long id;

    public Entity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

