package com.luxoft.dnepr.courses.regular.unit6.exeptions;

/**
 *
 */
public class EntityAlreadyExistException extends RuntimeException {

    public EntityAlreadyExistException() {
        super();
    }

    public EntityAlreadyExistException(String message, Throwable cause) {
        super(message, cause);
    }

    public EntityAlreadyExistException(String message) {
        super(message);
    }

    public EntityAlreadyExistException(Throwable cause) {
        super(cause);
    }
}
