<%@ page import="java.util.*" %>
<%@ page import="com.luxoft.dnepr.courses.unit16.SessionData" %>

<%
    String role = (String) session.getAttribute("role");
    if (role != null && role.equals("admin")) {

        SessionData data = (SessionData) request.getAttribute("data");

        out.print("<br>ActiveSessions : " + data.getActiveSessions());
        out.print("<br>ActiveSessionsUsers : " + data.getActiveSessionsUsers());
        out.print("<br>ActiveSessionsAdmins : " + data.getActiveSessionsAdmins());
        out.print("<br>TotalHttpRequests : " + data.getTotalHttpRequests());
        out.print("<br>TotalPostHttpRequests : " + data.getTotalPostHttpRequests());
        out.print("<br>TotalGetHttpRequests : " + data.getTotalGetHttpRequests());
        out.print("<br>TotalOtherHttpRequests : " + data.getTotalOtherHttpRequests());
    } else {
        response.sendRedirect("/index.jsp");
    }
%>

