package com.luxoft.dnepr.courses.unit16;

public class SessionData {
    long ActiveSessions;
    long ActiveSessionsUsers;
    long ActiveSessionsAdmins;
    long TotalHttpRequests;
    long TotalPostHttpRequests;
    long TotalGetHttpRequests;
    long TotalOtherHttpRequests;

    public SessionData() {
    }

    public long getActiveSessions() {
        return ActiveSessions;
    }

    public void setActiveSessions(long activeSessions) {
        ActiveSessions = activeSessions;
    }

    public long getActiveSessionsUsers() {
        return ActiveSessionsUsers;
    }

    public void setActiveSessionsUsers(long activeSessionsUsers) {
        ActiveSessionsUsers = activeSessionsUsers;
    }

    public long getActiveSessionsAdmins() {
        return ActiveSessionsAdmins;
    }

    public void setActiveSessionsAdmins(long activeSessionsAdmins) {
        ActiveSessionsAdmins = activeSessionsAdmins;
    }

    public long getTotalHttpRequests() {
        return TotalHttpRequests;
    }

    public void setTotalHttpRequests(long totalHttpRequests) {
        TotalHttpRequests = totalHttpRequests;
    }

    public long getTotalPostHttpRequests() {
        return TotalPostHttpRequests;
    }

    public void setTotalPostHttpRequests(long totalPostHttpRequests) {
        TotalPostHttpRequests = totalPostHttpRequests;
    }

    public long getTotalGetHttpRequests() {
        return TotalGetHttpRequests;
    }

    public void setTotalGetHttpRequests(long totalGetHttpRequests) {
        TotalGetHttpRequests = totalGetHttpRequests;
    }

    public long getTotalOtherHttpRequests() {
        return TotalOtherHttpRequests;
    }

    public void setTotalOtherHttpRequests(long totalOtherHttpRequests) {
        TotalOtherHttpRequests = totalOtherHttpRequests;
    }
}