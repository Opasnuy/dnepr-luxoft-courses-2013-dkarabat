package com.luxoft.dnepr.courses.unit16.listener;

import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import java.util.concurrent.atomic.AtomicLong;

public class AttributeSessionCountListener implements HttpSessionAttributeListener {
    @Override
    public void attributeAdded(HttpSessionBindingEvent sbe) {
        String role = (String)sbe.getSession().getAttribute("role");
        if(role.equals("admin"))
            getActiveAdminSessions(sbe).getAndIncrement();
        else if(role.equals("user"))
            getActiveUserSessions(sbe).getAndIncrement();
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent sbe) {
    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent sbe) {

    }
    private AtomicLong getActiveAdminSessions(HttpSessionBindingEvent sbe) {
        return (AtomicLong) sbe.getSession().getServletContext().getAttribute(Constants.ACTIVE_SESSION_ADMIN_ATTRIBUTE);
    }
    private AtomicLong getActiveUserSessions(HttpSessionBindingEvent sbe) {
        return (AtomicLong) sbe.getSession().getServletContext().getAttribute(Constants.ACTIVE_SESSION_USER_ATTRIBUTE);
    }
}
