package com.luxoft.dnepr.courses.unit16.utils;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.luxoft.dnepr.courses.unit16.User;

import java.util.Set;

public class LoginCheck {
    private Set<User> users;

    public void getUsersData(String json) {
        Gson gson = new Gson();
        users = gson.fromJson(json, new TypeToken<Set<User>>() {
        }.getType());
    }

    public boolean isRegisteredUser(User user) {
        boolean registered = false;
        if (users.contains(user)) {
            registered = true;
        }
        return registered;
    }

    public String getUsersRole(String login) {
        for (User user : users) {
            if (user.getLogin().equals(login))
                return user.getRole();
        }
        return "";
    }
}