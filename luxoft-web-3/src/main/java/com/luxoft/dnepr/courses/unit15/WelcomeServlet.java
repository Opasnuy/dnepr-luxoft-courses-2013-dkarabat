package com.luxoft.dnepr.courses.unit15;

import com.luxoft.dnepr.courses.unit15.util.PageBuilder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;


public class WelcomeServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        StringBuilder content = new StringBuilder("");
        HttpSession session = request.getSession(false);
        if(session != null && !session.isNew()) {
            String login = (String) session.getAttribute("login");
            content.append(PageBuilder.getWelcomePage(login));
            PrintWriter writer = response.getWriter();
            writer.println(content);
        } else {
            response.sendRedirect("/index.html");
        }

    }
}
