package com.luxoft.dnepr.courses.unit15.filter;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.luxoft.dnepr.courses.unit15.model.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Set;


public class AdminFilter implements Filter {
    private Set<User> users;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        String json = filterConfig.getServletContext().getInitParameter("users");
        Gson gson = new Gson();
        users = gson.fromJson(json, new TypeToken<Set<User>>() {
        }.getType());
    }

    @Override
    public void doFilter(ServletRequest servletRequest
            , ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(false);

        if (!isAdmin((String) session.getAttribute("login"))) {
            response.sendRedirect("/user");
            return;
        }
        filterChain.doFilter(request, response);
    }

    private boolean isAdmin(String login) {
        for (User user : users) {
            if (user.getLogin().equals(login) && user.getRole().equals("admin")) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void destroy() {
    }

}