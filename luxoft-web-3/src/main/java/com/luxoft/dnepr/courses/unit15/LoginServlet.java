package com.luxoft.dnepr.courses.unit15;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.luxoft.dnepr.courses.unit15.model.User;
import com.luxoft.dnepr.courses.unit15.util.PageBuilder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;


public class LoginServlet extends HttpServlet {

    private Set <User> users;

    private final boolean REGISTERED = true;
    private final boolean NOT_REGISTERED = false;

    @Override
    public void init() throws ServletException {
        String json = getServletContext().getInitParameter("users");
        Gson gson = new Gson();
        users = gson.fromJson(json, new TypeToken<Set<User>>() {
        }.getType());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        StringBuilder content = new StringBuilder("");
        content.append(PageBuilder.getIndexPage(REGISTERED));
        PrintWriter writer = response.getWriter();
        writer.println(content);
    }


    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        StringBuilder content = new StringBuilder("");
        String login = request.getParameter("log");
        String password = request.getParameter("pwd");
        User user = new User(login, password);
        if (isRegisteredUser(user)) {
            HttpSession session = request.getSession(true);
            session.setAttribute("login", request.getParameter("log"));
            session.setAttribute("role", getUsersRole(request.getParameter("login")));
            response.sendRedirect("/users");
        } else {
            content.append(PageBuilder.getIndexPage(NOT_REGISTERED));
            PrintWriter writer = response.getWriter();
            writer.println(content);
        }
    }

    private boolean isRegisteredUser(User user) {
        boolean registered = false;
        if (users.contains(user)) {
            registered = true;
        }
        return registered;
    }

    private String getUsersRole(String login) {
        for (User user : users) {
            if (user.getLogin().equals(login))
                return user.getRole();
        }
        return "";
    }

}