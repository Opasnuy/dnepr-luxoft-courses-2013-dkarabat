  $(function() {
    $.fn.autoClear = function () {
        // ��������� �� ���������� ���������� ������� ��������
        $(this).each(function() {
            $(this).data("mySearch", $(this).attr("value"));
        });
        $(this)
            .bind('focus', function() {   // ��������� ������
                if ($(this).attr("value") == $(this).data("mySearch")) {
                    $(this).attr("value", "").addClass('autoclear-normalcolor');
                }
            })
            .bind('blur', function() {    // ��������� ������ ������
                if ($(this).val() == "") {
                    $(this).attr("value", $(this).data("mySearch")).removeClass('autoclear-normalcolor');
                }
            });
        return $(this);
    }
});

$(function() {
    // ����������� ������ �� ���� ��������� � ������� "autoclear"    
    $('.mySearch').autoClear();
});