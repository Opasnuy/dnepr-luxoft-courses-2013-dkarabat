package com.luxoft.dnepr.courses.toprank;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;

public class ContentWorker implements Callable<Map<String, String>>{
    private String pageUrl;

    public ContentWorker(String pageUrl) {
        this.pageUrl = pageUrl;
    }

    @Override
    public Map<String, String> call() throws Exception {
        Map<String, String> contentList = new ConcurrentHashMap<>();
        URL url = null;
        try {
            url = new URL(pageUrl);
            URLConnection conn = url.openConnection();
            StringBuilder sb = new StringBuilder();
            try (BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()))) {
                String inputLine;
                while ((inputLine = br.readLine()) != null) {
                    sb.append(inputLine);
                }
            }
            contentList.put(pageUrl, sb.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return contentList;
    }
}
