package com.luxoft.dnepr.courses.toprank;

import java.util.*;
import java.util.concurrent.*;

public class TopRankResults {
    private Map<String, List<String>> graph = new ConcurrentHashMap<>();
    private Map<String, List<String>> reverseGraph = new ConcurrentHashMap<>();
    private Map<String, List<String>> index = new ConcurrentHashMap<>();
    private Map<String, Double> ranks = new ConcurrentHashMap<>();

    public Map<String, List<String>> getGraph() {
        return graph;
    }

    public Map<String, List<String>> getReverseGraph() {
        return reverseGraph;
    }

    public Map<String, List<String>> getIndex() {
        return index;
    }

    public Map<String, Double> getRanks() {
        return ranks;
    }

    public void setGraph(Map<String, String> urlContent) {
        Map<String, List<String>> links = getLinks(urlContent);

        for (Map.Entry<String, List<String>> entry : links.entrySet()) {
            graph.putAll(addLinks(entry.getKey(), links));
        }
    }

    public void setReverseGraph(Map<String, String> urlContent) {
        reverseGraph.putAll(getLinks(urlContent));
    }


    public void setIndex(Map<String, String> urlContent) {
        HashMap<String, ArrayList<String>> words = dividePageOnWords(urlContent);
        ArrayList<String> wordList = new ArrayList<String>();

        for (Map.Entry<String, ArrayList<String>> entry : words.entrySet()) {
            wordList.addAll(entry.getValue());
        }

        for (String str : wordList) {
            List<String> urls = new ArrayList<String>();
            for (Map.Entry<String, ArrayList<String>> entry : words.entrySet()) {
                for (String val : entry.getValue()) {
                    if (val.equals(str)) {
                        urls.add(entry.getKey());
                    }
                }
//                if (entry.getValue().contains(str)) {
//                    urls.add(entry.getKey());
//                }
            }
            index.put(str, urls);
        }
    }

    public void setRanks(Map<String, Double> ranks) {
        this.ranks = ranks;
    }

    private HashMap<String, ArrayList<String>> dividePageOnWords(Map<String, String> urlContent) {
        HashMap<String, ArrayList<String>> words = new HashMap<String, ArrayList<String>>();

        for (Map.Entry<String, String> entry : urlContent.entrySet()) {
            words.put(entry.getKey(), extractWordsFromPages(entry.getValue()));
        }
        return words;
    }

    private ArrayList<String> extractWordsFromPages(String value) {
        ArrayList<String> list = new ArrayList<>();
        ExecutorService es1 = Executors.newFixedThreadPool(5);
        Future<ArrayList<String>> f1 = es1.submit(new IndexWorker(value));
        while (!f1.isDone()) {
        }
        try {
            list.addAll(f1.get());
        } catch (InterruptedException ie) {
            ie.printStackTrace(System.err);
        } catch (ExecutionException ee) {
            ee.printStackTrace(System.err);
        }
        es1.shutdown();
        return list;
    }

    private List<String> extractUrls(String value) {
        ArrayList<String> list = new ArrayList<>();
        ExecutorService es1 = Executors.newFixedThreadPool(5);
        Future<List<String>> f1 = es1.submit(new GraphWorker(value));
        while (!f1.isDone()) {
        }
        try {
            list.addAll(f1.get());
        } catch (InterruptedException ie) {
            ie.printStackTrace(System.err);
        } catch (ExecutionException ee) {
            ee.printStackTrace(System.err);
        }
        es1.shutdown();
        return list;

    }

    public Map<String, List<String>> getLinks(Map<String, String> urlContent) {
        Map<String, List<String>> tmp = new HashMap();
        for (Map.Entry<String, String> entry : urlContent.entrySet()) {
            tmp.put(entry.getKey(), extractUrls(entry.getValue()));
        }
        return tmp;
    }

    private Map<String, List<String>> addLinks(String value, Map<String, List<String>> tmp) {
        List<String> result = new ArrayList<String>();
        Map<String, List<String>> tmpGraph = new HashMap();
        for (Map.Entry<String, List<String>> entry : tmp.entrySet()) {
            if (entry.getValue().contains(value)) {
                result.add(entry.getKey());
            }
        }
        tmpGraph.put(value, result);
        return tmpGraph;
    }
}