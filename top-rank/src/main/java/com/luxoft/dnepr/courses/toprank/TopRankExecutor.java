package com.luxoft.dnepr.courses.toprank;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TopRankExecutor {
    private double dampingFactor;
    private int numberOfLoopsInRankComputing;

    public TopRankExecutor(double dampingFactor, int numberOfLoopsInRankComputing) {
        this.dampingFactor = dampingFactor;
        this.numberOfLoopsInRankComputing = numberOfLoopsInRankComputing;
    }

    public TopRankResults execute(Map<String, String> urlContent) {
        TopRankResults results = new TopRankResults();
        results.setGraph(urlContent);
        results.setIndex(urlContent);
        results.setReverseGraph(urlContent);

        Map<String, Double> ranks = new HashMap<String, Double>();
        Map<String, Double> tmpRanks = new HashMap<String, Double>();
        double rank = 1.0 / urlContent.size();

        for (Map.Entry<String, String> entry : urlContent.entrySet()) {
            ranks.put(entry.getKey(), rank);
        }
        tmpRanks.putAll(ranks);

        for (int i = 0; i < numberOfLoopsInRankComputing; i++) {

            for (Map.Entry<String, List<String>> entry : results.getGraph().entrySet()) {
                double tmp = (1 - dampingFactor) / results.getGraph().size();
                if (entry.getValue().size() == 0) {
                    ranks.put(entry.getKey(), tmp);
                } else {
                    rank = tmp + getSumm(entry.getKey(), results, tmpRanks, urlContent);
                    ranks.put(entry.getKey(), rank);
                }
            }

            for (Map.Entry<String, Double> entry : ranks.entrySet()) {
                tmpRanks.put(entry.getKey(), entry.getValue());
            }
        }
        results.setRanks(ranks);
        return results;
    }

    private double getSumm(String key, TopRankResults results, Map<String, Double> ranks, Map<String, String> urlContent) {
        double summ = 0.0;
        for (String str : results.getGraph().get(key)) {
            summ += dampingFactor * ranks.get(str) / results.getLinks(urlContent).get(str).size();
        }
        return summ;
    }
}