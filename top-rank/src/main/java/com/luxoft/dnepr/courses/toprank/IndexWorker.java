package com.luxoft.dnepr.courses.toprank;


import java.util.ArrayList;
import java.util.concurrent.Callable;

public class IndexWorker implements Callable<ArrayList<String>> {

    private String content;

    public IndexWorker(String content) {
        this.content = content;
    }


    @Override
    public ArrayList<String> call() throws Exception {
        ArrayList<String> wordList = new ArrayList<String>();
        String[] words = content.split("[\t\n\\s]");
        for (int i = 0; i < words.length; i++) {
            wordList.add(words[i]);
        }
        return wordList;
    }
}
