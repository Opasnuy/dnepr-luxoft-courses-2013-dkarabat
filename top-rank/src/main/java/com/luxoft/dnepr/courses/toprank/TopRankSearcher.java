package com.luxoft.dnepr.courses.toprank;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

public class TopRankSearcher {

    private static final double DEFAULT_DAMPING_FACTOR = 0.8;
    private static final int DEFAULT_NUMBER_OF_LOOPS_IN_RANK_COMPUTING = 10;
    private static final int CONTENT_THREADS_NUMBER = 6;


    public TopRankResults execute(List<String> urls) {
        return execute(urls, DEFAULT_DAMPING_FACTOR, DEFAULT_NUMBER_OF_LOOPS_IN_RANK_COMPUTING);
    }


    public TopRankResults execute(List<String> urls, double dampingFactor, int numberOfLoopsInRankComputing) {
        TopRankResults results = new TopRankResults();
        Map<String, String> urlContent = getPageContent(urls);

        results.setGraph(urlContent);
        results.setIndex(urlContent);
        results.setReverseGraph(urlContent);

        Map<String, Double> ranks = new HashMap<String, Double>();
        Map<String, Double> tmpRanks = new HashMap<String, Double>();
        double rank = 1.0 / urlContent.size();

        for (Map.Entry<String, String> entry : urlContent.entrySet()) {
            ranks.put(entry.getKey(), rank);
        }
        tmpRanks.putAll(ranks);

        for (int i = 0; i < numberOfLoopsInRankComputing; i++) {

            for (Map.Entry<String, List<String>> entry : results.getGraph().entrySet()) {
                double tmp = (1 - dampingFactor) / results.getGraph().size();
                if (entry.getValue().size() == 0) {
                    ranks.put(entry.getKey(), tmp);
                } else {
                    rank = tmp + getSumm(entry.getKey(), results, tmpRanks, urlContent, dampingFactor);
                    ranks.put(entry.getKey(), rank);
                }
            }

            for (Map.Entry<String, Double> entry : ranks.entrySet()) {
                tmpRanks.put(entry.getKey(), entry.getValue());
            }
        }
        results.setRanks(ranks);
        return results;
    }

    private double getSumm(String key, TopRankResults results, Map<String, Double> ranks,
                           Map<String, String> urlContent, double dampingFactor) {
        double summ = 0.0;
        for (String str : results.getGraph().get(key)) {
            summ += dampingFactor * ranks.get(str) / results.getLinks(urlContent).get(str).size();
        }
        return summ;
    }

    private Map<String, String> getPageContent(List<String> urls) {

        Map<String, String> contentList = new ConcurrentHashMap<>();
        ExecutorService poolThr = Executors.newFixedThreadPool(CONTENT_THREADS_NUMBER);
        List<Future<Map<String, String>>> list = new ArrayList();

        for (String page : urls) {
            Future<List<String>> f1 = poolThr.submit(new GraphWorker(page));
            ContentWorker callable = new ContentWorker(page);
            Future<Map<String, String>> future = poolThr.submit(callable);
            list.add(future);
        }
        for (Future<Map<String, String>>  fut : list)
            try {
                contentList.putAll(fut.get());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        poolThr.shutdown();
        return contentList;
    }
}
