package com.luxoft.dnepr.courses.toprank;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GraphWorker implements Callable<List<String>> {

    private String content;

    public GraphWorker(String content) {
        this.content = content;
    }


    @Override
    public List<String> call() throws Exception {
       List<String> result = new ArrayList<String>();
        if (content == null) {
            throw new NullPointerException();
        }
        String urlPattern = "((https?|ftp|gopher|telnet|file):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)";
        Pattern p = Pattern.compile(urlPattern, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(content);
        while (m.find()) {
            result.add(content.substring(m.start(0), m.end(0)));
        }
        return result;
    }
}
