CREATE TABLE IF NOT EXISTS makers (
  maker_id     INT,
  maker_name   VARCHAR(50) NOT NULL,
  maker_adress VARCHAR(200),
  CONSTRAINT pk_makers PRIMARY KEY (maker_id)
);

INSERT INTO makers (maker_id, maker_name, maker_adress) VALUES (1, 'A', 'AdressA');
INSERT INTO makers (maker_id, maker_name, maker_adress) VALUES (2, 'B', 'AdressB');
INSERT INTO makers (maker_id, maker_name, maker_adress) VALUES (3, 'C', 'AdressC');
INSERT INTO makers (maker_id, maker_name, maker_adress) VALUES (4, 'D', 'AdressD');
INSERT INTO makers (maker_id, maker_name, maker_adress) VALUES (5, 'E', 'AdressE');


ALTER TABLE product ADD COLUMN maker_id INT;

UPDATE product p, (SELECT * FROM makers) m SET p.maker_id = m.maker_id WHERE m.maker_name = p.maker;

ALTER TABLE product DROP COLUMN maker,
MODIFY COLUMN maker_id INT NOT NULL,
ADD CONSTRAINT fk_product_makers FOREIGN KEY (maker_id) REFERENCES makers (maker_id);


CREATE TABLE IF NOT EXISTS printer_type (
  type_id   INT,
  type_name VARCHAR(50) NOT NULL,
  CONSTRAINT pk_printer_type PRIMARY KEY (type_id)
);

INSERT INTO printer_type (type_id, type_name) VALUES (1, 'Matrix');
INSERT INTO printer_type (type_id, type_name) VALUES (2, 'Laser');
INSERT INTO printer_type (type_id, type_name) VALUES (3, 'Jet');


ALTER TABLE printer ADD COLUMN type_id INT;

UPDATE printer p, (SELECT * FROM printer_type) t SET p.type_id = t.type_id WHERE t.type_name = p.type;

ALTER TABLE printer DROP type,
MODIFY COLUMN type_id INT NOT NULL,
ADD CONSTRAINT fk_printer_type FOREIGN KEY (type_id) REFERENCES printer_type (type_id);


ALTER TABLE printer MODIFY color CHAR(1) NOT NULL DEFAULT 'y';

CREATE INDEX ind_pc_price ON pc (price);
CREATE INDEX ind_laptop_price ON laptop (price);
CREATE INDEX ind_printer_price ON printer (price);


