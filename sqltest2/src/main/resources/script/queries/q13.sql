SELECT DISTINCT b.type, a.model, a.speed FROM laptop AS a, product AS b WHERE speed <
(SELECT MIN(speed) FROM pc ) AND b.type = 'Laptop'