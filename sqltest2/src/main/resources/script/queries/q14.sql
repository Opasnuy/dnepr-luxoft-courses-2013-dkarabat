SELECT DISTINCT maker_name, price FROM product AS a INNER JOIN printer AS b, makers AS c WHERE a.model = b.model
AND a.maker_id = c.maker_id
AND price = (SELECT MIN(price) FROM printer WHERE color ='y') AND color = 'y'