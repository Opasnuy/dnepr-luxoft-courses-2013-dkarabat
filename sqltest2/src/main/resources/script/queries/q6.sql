SELECT DISTINCT maker_name FROM makers AS a LEFT JOIN product AS b ON a.maker_id = b.maker_id
WHERE type ='PC' AND a.maker_id NOT IN (SELECT DISTINCT maker_id FROM product WHERE type = 'Laptop')